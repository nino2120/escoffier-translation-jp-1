---
title: エスコフィエ『料理の手引き』全注解
author:
- 五 島　学（責任編集・訳・注釈）
- 河井 健司（訳・注釈）
- 春野 裕征（訳）
- 山 本　学（訳）
- 高 橋　昇（校正）
pandoc-latex-environment:
    main: [main]
    recette: [recette]
    frsubenv: [frsubenv]
    frsecenv: [frsecenv]
    frsecbenv: [frsecbenv]
    frchapenv: [frchapenv]
...



<div class="main">

### アスパラガス {#asperges}

<div class="frsecbenv">Asperges</div>

アスパラガスは主に4種類[^1303_1]に分けられる。フランス産アスパラガスの典型
的な品種、アルジャントゥイユ[^1303_2]。グリーンアスパラガス[^1303_3]。ジェノヴァ
産紫アスパラガス[^1303_4]、イタリア産アスパラガスの典型で繊細な風味だがやや
えぐ味がある。ベルギー産ホワイトアスパラガス[^1303_5]、これも繊細な風味だが、
輸送による劣化がはげしい。

アスパラガスは出来るだけ新鮮なものを用いること。丁寧に皮をむき、手早く
洗う。紐で束ねてたっぷりの塩湯で茹でる。

種類によっては多少エグ味があるので、茹であがったらすぐに水を替えてエグ
味を取り除く。少なくともエグ味を弱めることは可能だ。茹でたアスパラガス
は専用の銀の網の上に盛るか、ナフキンの上に盛り付ける。

[^1303_1]: variété （ヴァリエテ）野菜についての場合通常は「品種」と訳すが、
    ここではバラエティ、種類くらいの意。なお、いわゆる「アスパラソバー
    ジュ」asperges sauvages （アスペルジュソヴァージュ）はまったくの別
    種であり、ここには含まれていない。栽培種としてのアスパラガスの歴史
    は古く、古代ローマまで遡れるとされている。4〜5世紀ごろに成立した
    「アピキウス」においても、「アスパラガスは乾かし、熱湯で固めにさっ
    と茹でる」とある(III-72)。また、ベルトラン・ゲガンの訳注によれば、
    ラヴェンナ産がもっとも珍重され、アフリカ産のもののほうが大きかった
    にもかかわらず、人気だったという。、また、かなり固めに茹で上げるの
    が好まれたという。ラヴェンナはフィレンツェ、ヴェネチアからそれぞれ
    約100kmのアドリア海側に位置する。ローマなどへの輸送の際、アスパラ
    ガスは水に漬けられていたのではないだろうか。だとすればアピキウスの
    記述にある「乾かす」も得心のいくところだろう。さて、プリニウスによ
    れば、栽培種のアスパラガスは胃によく、催淫作用もあるということだっ
    た(III-66, p.64n)。その後、中世の料理書で言及されることもなく、姿
    を消したかのように見えたが、16世紀末のオリヴィエ・ド・セール『農業
    経営論』においては、アーティチョークと同様に重要な野菜として扱われ
    ており、栽培に適した土壌が肥持ちのいい砂壌土であるくことなど詳しく
    論じられている。また、茎の部分15cm程度を軟白する栽培方法が示されて
    いる（pp.833-384)。17世紀になると、1651年のラ・ヴァレーヌフランス
    料理の本』において、オランデーズソースの原型ともいえる「白いソース」
    のレシピが中心となっている、アスパラガス・白いソース添え、が掲載さ
    れるにようになり(p.238)、この野菜の人気が復活したことがうかがえる
    （[オランデーズソース](#sauce-hollandaise)訳注も参照）。

[^1303_2]: Argenteuil （アルジョントゥイユ）。パリ近郊の地名。かつてアスパ
    ラガスの生産が盛んだったという。この地名を冠した品種が21世紀に
    なった現在でも主流であることは事実。穂先がやや紫がかる傾向にあるが
    基本的には緑色の品種。

[^1303_3]: 現在ではオランダの種苗会社が交配したF1品種が増えている。

[^1303_4]: 同上。ただしこの紫色は茹でると失なわれる。

[^1303_5]: ホワイトアスパラガスは「品種」ではなく栽培方法が異なる（軟白の工
    程が入る）。理屈のうえではどんな品種であってもホワイトアスパラガス
    にすることは可能。秋のうちに地上部を切り落し、根株を中心にプラウ
    （鋤の一種）などを用いて土を盛り上げる。全体に大きなかまぼこのよう
    な格好になる。春になると、地中深くの根株から伸びてきたアスパラガス
    の茎は日光に当たっていないので軟白されている。それを地上に出る直前
    に収穫する。収穫は一定期間で終了させ、盛り上げた土を平らに戻し、緑
    の茎葉を茂らせて翌年のために根株を養成する。トラクタがプラウを曳け
    るようかなり条間を広くとる必要があり、単位面積あたりの収量は低い。
    かつては缶詰の材料として北海道で盛んに栽培されていたが、だんだん生
    産量が落ちている。近年日本ではハウス栽培で土盛りをせずトンネルに遮
    光率100％のシートで暗闇を作って栽培する方式が増えつつある。いずれ
    もフランス料理においてあまり高い評価を得られていないのは、品種の選
    定と栽培方法に負うところが大きいだろう。

</div><!--endMain-->

<div class="recette">

#### アスパラガス・フランドル風[^1303_6] {#asperges-a-la-flamande}

<div class="frsubenv">Asperges à la Flamande</div>

フランドル地方では慣習的に、1人あたり熱々の半熟卵1個とバター 30 gを添える。

食べ手、半熟の黄身を自分でつぶして、塩こしょうで調味し、バターを加えて
食する。

もちろん、この半熟卵とバターのソースをあらかじめ作っておき、ソース入れ
で添えて供してもいい。



[^1303_6]: 現在のベ゙ルギー西部からフランス北部にかけて の北海に面する地
    域。フランダース。[ガルニチュール・フランドル風](#garniture-a-la-flamande)
    訳注参照。本文ではとくに指定されてい
    ないが、概説部分に「ベルギー産ホワイトアスパラガス」の説明があるこ
    と、初版の料理名が「ホワイトアスパラガス・フランドル風」であること
    を考慮する必要がある。

\atoaki{}


#### アスパラガスのグラタン {#asperges-au-gratin}

<div class="frsubenv">Asperges au Gratin</div>

\index{asperge@asperges!gratin@--- au Gratin}
\index{gratin@gratin!asperges@Aspergers au ---}
\index{あすはらかす@アスパラガス!くらたん@---のグラタン}
\index{くらたん@グラタン!あすはらかす@アスパラガスの---}
\srcMornay{asperges gratin}{Asperges au Gratin}{あすはらかすのくらたん}{アスパラガスのグラタン}


アスパラガスは穂先に[ソース・モルネー](#sauce-mornay)を薄く塗ってから
整列するように皿に盛る。アスパラガス全部を並べ終えたら、下から
$\frac{2}{3}$ 程度をバターを塗った紙で覆う。紙に覆われていない穂先のほ
うにソース・モルネーをたっぷり多いかけ、おろしたパルメザンチーズを振る。
強火のサラマンダーに入れてこんがり焼き色を付ける。覆っていた紙を取り除
き、すぐに供する。


\atoaki{}


#### アスパラガス・ミラノ風 {#asperges-a-la-milanaise}

\index{asperge@asperges!milanaise@--- à la MIlanaise}
\index{milanais@milanais(e)!asperges@Aspergers à la ---}
\index{あすはらかす@アスパラガス!みらのふう@---・ミラノ風}
\index{みらのふう@ミラノ風!あすはらかす@アスパラガス・---}


<div class="frsubenv">Asperges au Gratin</div>

茹でたアスパラガスはよく水気をきり、バターを塗った長い皿に、おろしたパ
ルメザンチーズを振りかけながら並べていく。アスパラガスの列を積み上げる
ように盛り、その各段で穂先にパルメザンをまぶすようにする。提供直前に、
チーズをまぶしあ部分に焦がしバターをたっぷりかけ、サラマンダーで軽く焼
き色を漬ける。

\atoaki{}

#### アスパラガス・モルネー {#asperges-mornay}

<div class="frsubenv">Asperges Mornay</div>

\index{asperge@asperges!mornay@--- Mornay|see Asperges au Gratin}
\index{mornay@Mornay!asperge@Asperes Mornay|see asperges au Gratin}
\index{あすはらかす@アスパラガス!もるねー@--- ・モルネー}
\index{もるねー@モルネー!あすはらかす@アスパラガス・---|see アスパラガスのグラタン}

[アスパラガスのグラタン](#asperges-au-gratin)と同じ[^1303_7]。


[^1303_7]: 初版ではこの記述の順が逆にになっている。初版ではアスパラガス
    のグラタンの項に、アスパラガル・モルネーを参考にするべしとある
    (pp.646-647)。これはつまり、料理名としてどちらかといえばアスパラガ
    ス・モルネーのほうがより一般的であった可能性があるが、項目別アルファ
    ベット順配列という『料理の手引き』の原則からすると、グラタンのほう
    が先になるからという編集上の理由もあったと想像される。



\atoaki{}


#### アスパラガス・ポーランド風 {#asperges-a-la-polonaise}

<div class="frsubenv">Asperges à la Polonaise</div>

\index{asperge@asperges!polonaise@--- à la Polonaise}
\index{polonais@polonais(s)!asperge@Asperes à la ---e}
\index{あすはらかす@アスパラガス!ほーらんとふう@--- ・ポーランド風}
\index{ほーらんとふう@ポーランド風!@アスパラガス・---}

茹でたアスパラガスはよく水気をきる。これを長い皿に整列するように並べ、
穂先に固茹で卵の黄身をパセリのみじん切りを混ぜたものを振りかける。

提供直前に、バター 125 gに対して細かい生パン粉30 gを加えた焦がしバター
をアスパラガスの穂先にかける。



\atoaki{}


#### アスパラガス・さまざまなソースで {#asperges-avec-sauces-deverses}

<div class="frsubenv">Asperges avec Sauces diverses</div>

\index{asperge@asperges!sauces diverses@--- avec sauces diverses}
\index{sauces diverses@sauce diverses!asperge@Asperes avec  ---}
\index{あすはらかす@アスパラガス!さまさまなそーすて@--- ・さまざまなソースで}
\index{さまさまなそーす@さまざまなソース!あすはらかす@アスパラガス・---で}
\srcBeurre{asperges sauces diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}
\srcHollandaise{asperges sauces diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}
\srcMousseline{asperges sauce diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}
\srcMaltaise{asperges sauce diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}
\srcBearnaise{asperges sauce diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}
\srcMayonnaise{asperges sauce diverses}{Asperges avec Sauces diverses}{あすはらかすさまさまなそーすて}{アスパラガス・さまざまなソースで}



[ソース・オ・ブール](#sauce-au-beurre)、[オランデー
ズ](#sauce-hollandaise)、[ムスリーヌ](#sauce-mousseline)、[マルタ風ソー
ス](#sauce-maltaise)は、温製アスパラガスに添えられることがとても多い。
ハーブ類を加えていない[ソース・ベアルネーズ](#sauce-bearnaise)と溶かし
バターも同様に用いられることがある。

冷製の場合、植物油ベースのソース[^1303_8]か[マヨネーズ](#mayonnaise)を添え
る。とりわけマヨネーズにホイップクリームを加えたいわゆる**マヨネーズ・
シャンティイ**が用いられる。


[^1303_8]: [vinaigrette](#vinaigrette)や[レムラード](#sauce-remoulade)など。






\atoaki{}


#### アスパラガスの穂先・バター風味 {#pointes-d-asperges-au-beurre}

<div class="frsubenv">Pointes d'Asperges au beurre</div>

\index{asperge@asperges!pointes beurre@Pointes d'--- au beurre}
\index{beurre@beurre@pointes d'asperges!Pointes d'Asperges au ---}
\index{あすはらかすのほさき@アスパラガスの穂先!はたーふうみ@--- ・バター風味}
\index{はたーふうみ@バター風味!あすはらかすのほさき@アスパラガスの穂先}

アスパラガスの穂先はもっぱらガルニチュールとして、あるいはガルニチュー
ルの一部として用いられるが、野菜料理として供してもいい。

穂先は茎を付けて 5 cm程にカットする。これを束にしておく。残りの茎の柔
らかい部分はえんどう豆くらいの大きさにカットする。これを洗ってから塩湯
に投入し、強火で緑が鮮かになるよう茹でる。

火が通ったらすぐに取り出してしっかり湯をきる。冷水にはさらさないこと。
鍋に入れ火にかけて炒めながら水気をとばし、火から外してバターであえる。
これを野菜料理用深皿に入れ、その上に穂先の束を盛る。

これはブシェ[^1303_9]やタルトレットにアスパラガスの穂先を数本ずつ詰めて供す
ることがとてもよく行なわれている。




[^1303_9]: ヴォロヴァンに似たパイ生地で作ったケース。小さく作ったものはオー
    ドブルとして供された。


\atoaki{}


#### アスパラガスの穂先・クリーム風味 {#pointes-d-asperges-a-la-creme}

<div class="frsubenv">Pointes d'Asperges à la Crème</div>

\index{asperge@asperges!pointes creme@Pointes d'--- à la Crème}
\index{cremes@crème!pointes asperges@Pointes d'Asperges à la ---}
\index{あすはらかすのほさき@アスパラガスの穂先!くりーむふうみ@--- ・バター風味}
\index{くりーむふうみ@クリーム風味!あすはらかすのほさき@アスパラガスの穂先}


アスパラガスは前項と同様に仕込みをして茹でる。

アスパラガスを生クリームであえる要領は他の野菜の場合とおなじ。供する際
には上述のバター風味を同様にすればいい。

</div><!--endRecette-->



    
